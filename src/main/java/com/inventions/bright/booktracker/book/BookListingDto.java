package com.inventions.bright.booktracker.book;

import lombok.Data;

@Data
class BookListingDto {

    private Long id;
    private String title;
    private String author;
    private String isbn;
    private int numberOfPages;
    private double rate;
}
