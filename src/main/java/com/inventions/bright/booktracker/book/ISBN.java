package com.inventions.bright.booktracker.book;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE_USE;

@Constraint(validatedBy = ISBNValidator.class)
@Target({FIELD, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@interface ISBN {

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
