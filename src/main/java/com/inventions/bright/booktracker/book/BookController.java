package com.inventions.bright.booktracker.book;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@Slf4j
@Validated
@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
class BookController {

    private final BookService bookService;

    @PostMapping
    @ResponseStatus(CREATED)
    Long registerBook(@RequestBody @Valid RegisterBookDto dto) {
        log.info("Registering new book: {}", dto);
        return bookService.registerBook(dto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(OK)
    void deleteBook(@PathVariable long id) {
        log.info("Deleting book with id: {}", id);
        bookService.deleteBook(id);

    }

    @PutMapping("/{id}")
    @ResponseStatus(OK)
    Long updateBook(@PathVariable long id, @RequestBody @Valid RegisterBookDto dto) {
        log.info("Updating book with id: {}", id);
        return bookService.updateBook(id, dto);
    }

    @GetMapping
    List<BookListingDto> listBooks() {
        return bookService.listBooks();
    }

}
