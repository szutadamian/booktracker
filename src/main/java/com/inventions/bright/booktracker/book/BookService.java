package com.inventions.bright.booktracker.book;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Transactional
class BookService {

    private final BookMapper bookMapper;

    private final BookRepository bookRepository;
    
    Long registerBook(RegisterBookDto dto) {
        checkIfIsbnWasRegistered(dto.getIsbn());

        Book book = bookMapper.dtoToEntity(dto);
        return bookRepository.save(book).getId();
    }

    Long updateBook(long id, RegisterBookDto dto) {
        Book book = bookRepository.findById(id).orElseThrow(
                () -> new NotFoundException(format("Unable to find book with id %d", id)));

        if (hasDifferentIsbn(dto.getIsbn(), book.getIsbn())) {
            checkIfIsbnWasRegistered(dto.getIsbn());
        }

        bookMapper.updateBookFromDto(dto, book);
        return bookRepository.save(book).getId();
    }

    List<BookListingDto> listBooks() {
        List<Book> allBooks = bookRepository.findAll();
        return bookMapper.entityToDto(allBooks);
    }

    void deleteBook(long id) {
        try {
            bookRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(format("Unable to find book with id %d", id));
        }
    }

    private static boolean hasDifferentIsbn(String previousIsbn, String currentIsbn) {
        return !previousIsbn.equals(currentIsbn);
    }

    private void checkIfIsbnWasRegistered(String isbn) {
        if (isbnAlreadyRegistered(isbn)) {
            throw new ISBNAlreadyRegisteredException(format("ISBN %s was already registered.", isbn));
        }
    }

    private boolean isbnAlreadyRegistered(String isbn) {
        return bookRepository.existsByIsbn(isbn);
    }
}
