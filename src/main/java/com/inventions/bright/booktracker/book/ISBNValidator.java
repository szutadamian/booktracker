package com.inventions.bright.booktracker.book;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Character.getNumericValue;
import static java.lang.Character.isDigit;
import static java.util.Objects.isNull;

/**
 * I'm aware that there is already hibernate validator for isbn,
 * but for the purposes of this task i will implement it here
 */
public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    private static Map<Integer, ValidationStrategy> validationStrategies = new HashMap<>();
    static{
        validationStrategies.put(10, new ISBN10ValidationStrategy());
        validationStrategies.put(13, new ISBN13ValidationStrategy());
    }

    interface ValidationStrategy {
        boolean isValid(String isbn);
    }

    private static class ISBN10ValidationStrategy implements ValidationStrategy {

        @Override
        public boolean isValid(String isbn) {
            char[] chars = isbn.toCharArray();
            int sum = 0;
            for (int i = 0; i < 10; i++) {
                char singleChar = chars[i];
                if ('X' == singleChar && i == 9) {
                    sum += 10;
                } else if (isDigit(chars[i])){
                    sum += getNumericValue(chars[i]) * (10 - i);
                } else {
                    return false;
                }
            }
            return sum % 11 == 0;
        }
    }

    private static class ISBN13ValidationStrategy implements ValidationStrategy {

        @Override
        public boolean isValid(String isbn) {
            char[] chars = isbn.toCharArray();
            int sum = 0;
            for (int i = 0; i < 12; i++) {
                if (!isDigit(chars[i])) {
                    return false;
                }
                sum += (i % 2 * 2 + 1) * getNumericValue(chars[i]);
            }
            return sum % 10 == 0;
        }
    }

    @Override
    public boolean isValid(String isbn, ConstraintValidatorContext context) {
        boolean valid = validationStrategies.getOrDefault(defaultString(isbn).length(), input -> false)
                                            .isValid(isbn);

        if(!valid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate("Wrong ISBN format. Possible ISBN formats are: 10digits, 9digits + X or 13digits")
                   .addConstraintViolation();
            return false;
        }

        return true;
    }

    private static String defaultString(String isbn) {
        return isNull(isbn) ? "" : isbn;
    }
}
