package com.inventions.bright.booktracker.book;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {

    @Mapping(target = "id", ignore = true)
    Book dtoToEntity(RegisterBookDto dto);

    List<BookListingDto> entityToDto(List<Book> entities);

    @Mapping(target = "id", ignore = true)
    void updateBookFromDto(RegisterBookDto dto, @MappingTarget Book book);
}
