package com.inventions.bright.booktracker.book;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
class RegisterBookDto {

    @NotEmpty
    @Size(max = 255)
    private String title;

    @NotEmpty
    @Size(max = 255)
    private String author;

    @NotEmpty
    @ISBN
    private String isbn;

    @Min(1)
    private int numberOfPages;

    @Min(1)
    @Max(5)
    private double rate;
}
