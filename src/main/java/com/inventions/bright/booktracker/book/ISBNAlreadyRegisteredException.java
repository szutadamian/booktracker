package com.inventions.bright.booktracker.book;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ResponseStatus(BAD_REQUEST)
class ISBNAlreadyRegisteredException extends RuntimeException {
    ISBNAlreadyRegisteredException(String message) {
        super(message);
    }
}
