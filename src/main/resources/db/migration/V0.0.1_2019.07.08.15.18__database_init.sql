CREATE TABLE book (
	id bigserial NOT NULL,
	title VARCHAR(255) NOT NULL,
	author VARCHAR(255) NOT NULL,
	isbn VARCHAR(255) NOT NULL UNIQUE,
	number_of_pages SMALLINT,
	rate numeric
);
