package com.inventions.bright.booktracker.book

import com.inventions.bright.booktracker.IntegrationTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.web.context.WebApplicationContext
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static com.inventions.bright.booktracker.book.TestUtils.VALID_BOOK_PAYLOAD
import static com.inventions.bright.booktracker.book.TestUtils.getBookPayloadMap
import static groovy.json.JsonOutput.toJson
import static java.lang.Long.parseLong
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

@IntegrationTest
class BookControllerCreateIntTest extends Specification {

    @Autowired
    private WebApplicationContext context

    @Autowired
    private BookRepository bookRepository

    private MockMvc mvc

    def setup() {
        mvc = webAppContextSetup(context).build()
    }

    @Unroll
    def "should validate creation endpoint"(String author, String isbn, int numberOfPages, int rate, String title) {
        given:
        def dto = getBookPayloadMap(author, isbn, numberOfPages, rate, title)
        def payload = toJson(dto)
        when:
        ResultActions result = executeRestCall(payload)
        then:
        result.andExpect(status().isBadRequest())
        where:
        author    | isbn         | numberOfPages | rate | title
        null      | "0596520689" | 120           | 1    | "title"
        "author"  | null         | 120           | 1    | "title"
        "author"  | "0596520689" | -1            | 1    | "title"
        "author"  | "0596520689" | 0             | 1    | "title"
        "author"  | "0596520689" | 1             | 6    | "title"
        'a' * 256 | "0596520689" | 120           | 1    | "title"
        "author"  | "0596520689" | 120           | 1    | null
        "author"  | "0596520689" | 120           | 1    | 'a' * 256
    }

    def "should store valid book"() {
        given:
        def payload = toJson(VALID_BOOK_PAYLOAD)
        when:
        ResultActions result = executeRestCall(payload)
        then:
        result.andExpect(status().isCreated())

        and: "book stored correctly"
        def bookId = parseLong(result.andReturn().getResponse().getContentAsString())
        Book book = bookRepository.findById(bookId).get()

        then:
        book.author == VALID_BOOK_PAYLOAD.author
        book.title == VALID_BOOK_PAYLOAD.title
        book.rate == VALID_BOOK_PAYLOAD.rate
        book.numberOfPages == VALID_BOOK_PAYLOAD.numberOfPages
        book.isbn == VALID_BOOK_PAYLOAD.isbn
    }

    def "should validate if isbn was already registered"() {
        given:
        def payload = toJson(VALID_BOOK_PAYLOAD)
        when: "execute same request twice"
        executeRestCall(payload)
        def result = executeRestCall(payload)
        then:
        result.andExpect(status().isBadRequest())
    }

    private ResultActions executeRestCall(String payload) {
        mvc.perform(post("/api/books")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8)
                .content(payload))
    }

}
