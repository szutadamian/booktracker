package com.inventions.bright.booktracker.book

class TestUtils {

    static def VALID_BOOK_PAYLOAD = getBookPayloadMap("author", "0596520689", 120, 1, "title")

    static def getBookPayloadMap(String author, String isbn, int numberOfPages, double rate, String title) {
        [
                author: author,
                isbn : isbn,
                numberOfPages : numberOfPages,
                rate : rate,
                title : title
        ]
    }

}
