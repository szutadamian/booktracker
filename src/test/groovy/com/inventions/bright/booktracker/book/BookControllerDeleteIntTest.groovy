package com.inventions.bright.booktracker.book

import com.inventions.bright.booktracker.IntegrationTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import static com.inventions.bright.booktracker.book.TestUtils.VALID_BOOK_PAYLOAD
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

@IntegrationTest
class BookControllerDeleteIntTest extends Specification {

    @Autowired
    private WebApplicationContext context

    @Autowired
    private BookRepository bookRepository

    private MockMvc mvc
    private Book storedBook

    def setup() {
        mvc = webAppContextSetup(context).build()
        this.storedBook = bookRepository.save(VALID_BOOK_PAYLOAD as Book)
    }

    def "should remove book from database"() {
        given:
        long id = this.storedBook.id
        when:
        def response = executeRestCall(id)
        then:
        response.andExpect(status().isOk())

        and: "entity removed from DB"
        !bookRepository.findById(id).isPresent()
    }

    def "should throw exception when book wasn't found"() {
        given:
        long id = -1
        when:
        def response = executeRestCall(id)
        then:
        response.andExpect(status().isNotFound())
    }

    private ResultActions executeRestCall(long id) {
        mvc.perform(delete("/api/books/$id")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8))
    }

}
