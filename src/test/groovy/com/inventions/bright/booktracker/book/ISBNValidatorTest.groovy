package com.inventions.bright.booktracker.book


import spock.lang.Specification
import spock.lang.Unroll

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.ValidatorFactory

import static com.inventions.bright.booktracker.book.TestUtils.VALID_BOOK_PAYLOAD

class ISBNValidatorTest extends Specification {

    Validator validator

    def setup(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory()
        validator = factory.getValidator()
    }

    @Unroll
    def "should accept 9digits + X, 10 digits or 13 digits as isbn, isbn: #isbn"() {
        given:
        def book = VALID_BOOK_PAYLOAD as RegisterBookDto
        book.isbn = isbn
        when:
        Optional<ConstraintViolation<RegisterBookDto>> violation = validator.validate(book)
                .stream()
                .findFirst();
        then:
        !violation.isPresent()
        where:
        isbn << [
                "0596520689",
                "080442957X",
                "9783161484100",
                "0306406152",
                "0306406152",
                "0596520689",
                "0596520689"
        ]
    }

    @Unroll
    def "should not accept invalid isbn, isbn: #isbn"() {
        given:
        def book = VALID_BOOK_PAYLOAD as RegisterBookDto
        book.isbn = isbn
        when:
        Optional<ConstraintViolation<RegisterBookDto>> violation = validator.validate(book)
                .stream()
                .findFirst();
        then:
        violation.get()
        where:
        isbn << [
                "",
                null,
                " ",
                "0-5961-52068-9",
                "11 5122 52068 9",
                "0512520689",
                "ISBN-10- 0-596-52068-9",
                "080442957A",
                "08B4429579"
        ]
    }
}
