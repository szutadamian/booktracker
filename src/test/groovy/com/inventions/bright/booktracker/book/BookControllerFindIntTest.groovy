package com.inventions.bright.booktracker.book

import com.inventions.bright.booktracker.IntegrationTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import static com.inventions.bright.booktracker.book.TestUtils.VALID_BOOK_PAYLOAD
import static groovy.json.JsonOutput.toJson
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

@IntegrationTest
class BookControllerFindIntTest extends Specification {

    @Autowired
    private WebApplicationContext context

    @Autowired
    private BookRepository bookRepository

    private MockMvc mvc

    def setup() {
        mvc = webAppContextSetup(context).build()
    }

    def "should return added book"() {
        given:
        def dto = VALID_BOOK_PAYLOAD
        def storedBook = bookRepository.save(dto as Book)

        when: "now it should not be possible to modify previous isbn to just added one"
        def payload = toJson(dto)
        def result = executeRestCall(payload)
        then:
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$[0].isbn').value(storedBook.isbn))
        result.andExpect(jsonPath('$[0].title').value(storedBook.title))
        result.andExpect(jsonPath('$[0].author').value(storedBook.author))
        result.andExpect(jsonPath('$[0].rate').value(storedBook.rate))
        result.andExpect(jsonPath('$[0].numberOfPages').value(storedBook.numberOfPages))
        result.andExpect(jsonPath('$[0].id').value(storedBook.id))
    }

    private ResultActions executeRestCall(String payload) {
        mvc.perform(get("/api/books")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8)
                .content(payload))
    }

}
