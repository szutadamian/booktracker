package com.inventions.bright.booktracker.book

import com.inventions.bright.booktracker.IntegrationTest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import static com.inventions.bright.booktracker.book.TestUtils.VALID_BOOK_PAYLOAD
import static groovy.json.JsonOutput.toJson
import static java.lang.Long.parseLong
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup

@IntegrationTest
class BookControllerUpdateIntTest extends Specification {

    @Autowired
    private WebApplicationContext context

    @Autowired
    private BookRepository bookRepository

    private MockMvc mvc
    private Book storedBook

    def setup() {
        mvc = webAppContextSetup(context).build()
        this.storedBook = bookRepository.save(VALID_BOOK_PAYLOAD as Book)
    }

    def "should validate if isbn was already registered"() {
        given: "store book with different isbn"
        def dto = VALID_BOOK_PAYLOAD
        dto.isbn = "0306406152"
        bookRepository.save(dto as Book)

        when: "now it should not be possible to modify previous isbn to just added one"
        def payload = toJson(dto)
        def result = executeRestCall(payload, storedBook.id)
        then:
        result.andExpect(status().isBadRequest())
    }

    def "should update book isbn if isbn wasn't already registered"() {
        given:
        def dto = VALID_BOOK_PAYLOAD
        def newIsbn = "0306406152"
        dto.isbn = newIsbn
        def payload = toJson(dto)
        when:
        def result = executeRestCall(payload, storedBook.id)
        then:
        result.andExpect(status().isOk())

        and: "changes stored correctly"
        def bookId = parseLong(result.andReturn().getResponse().getContentAsString())
        Book book = bookRepository.findById(bookId).get()
        then:
        book.isbn == newIsbn
    }

    def "should return 404 if there was no entity to update"() {
        given:
        def dto = VALID_BOOK_PAYLOAD
        def newIsbn = "0306406152"
        dto.isbn = newIsbn
        def payload = toJson(dto)
        when:
        def result = executeRestCall(payload, -1)
        then:
        result.andExpect(status().isNotFound())
    }

    private ResultActions executeRestCall(String payload, Long id) {
        mvc.perform(put("/api/books/$id")
                .contentType(APPLICATION_JSON_UTF8)
                .accept(APPLICATION_JSON_UTF8)
                .content(payload))
    }

}
