package com.inventions.bright.booktracker;

import lombok.SneakyThrows;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;

class PostgresContainer extends PostgreSQLContainer<PostgresContainer>
        implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final String IMAGE_VERSION = "postgres:9.6";

    private static PostgresContainer container;

    private PostgresContainer() {
        super(IMAGE_VERSION);
    }

    @SneakyThrows
    public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        if (container == null) {
            container = new PostgresContainer()
                    .withDatabaseName("booktrackerTest")
                    .withUsername("postgres")
                    .withPassword("postgres");
        }
        container.start();
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }

}
