#!/usr/bin/env bash

docker build -t ipostgres .
docker run --rm -p 5432:5432 --name postgres -e POSTGRES_PASSWORD=postgres -d ipostgres
