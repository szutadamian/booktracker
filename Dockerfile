# vim:set ft=dockerfile:

FROM postgres:9.6

COPY create-schema.sql /docker-entrypoint-initdb.d/create-db.sql
