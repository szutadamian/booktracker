#### Start PostgresDB service
1. Start docker
2. Execute script: `./start.sh`

#### Start BookTracker Application
1. Execute in root folder: `mvn clean package`
2. Run `java -jar target/booktracker-0.0.1-SNAPSHOT.jar`

#### Postman Rest calls 
1. Import Postman collections from file postmanRequests.json
